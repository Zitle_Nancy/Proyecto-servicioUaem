create database portal;
	use portal;

create table ingresar(
id_personas int not null auto_increment primary key, 
Nombre varchar(45) not null,
Apellidos varchar(45) not null unique,
Lic varchar(45) not null,
Correo varchar(45) not null unique,
Pw varchar(30) not null,
pw1 varchar(30) not null
);

CREATE TABLE  `tbl_pdf` (
`id_documento` int(10)  NOT NULL AUTO_INCREMENT,
  `titulo` varchar(150) not NULL,
  `descripcion` varchar(80),
  `tamanio` int(10) unsigned DEFAULT NULL,
  `tipo` varchar(150) DEFAULT NULL,
  `nombre_archivo` varchar(255) DEFAULT NULL,
   PRIMARY KEY (`id_documento`)
);	


create table link_ico(
id_link int not null auto_increment primary key, 
titulo varchar(30) not null,
descripcion varchar(45) not null,
ruta varchar(45) not null
);