<!DOCTYPE HTML>
<?php
include_once 'config.inic.php';
if (isset($_POST['subir_btn'])) {
    $nombre = $_FILES['archivolibro']['name'];
    $tipo = $_FILES['archivolibro']['type'];
    $tamanio = $_FILES['archivolibro']['size'];
    $ruta = $_FILES['archivolibro']['tmp_name'];
    $destino = "libros/". $nombre;
    if ($tipo == "application/pdf") {
        if (copy($ruta, $destino)) {
            $titulo= $_POST['titulo'];
            $descri= $_POST['descripcion'];
            $db=new Conect_MySql();
            $sql = "INSERT INTO tbl_pdf(titulo,descripcion,tamanio,tipo,nombre_archivo) VALUES('$titulo','$descri','$tamanio','$tipo','$nombre')";
            $query = $db->execute($sql);
            if($query){
                print "<script>alert(\"Tu archivo fue subido exitosamente\"); window.location='agradecimiento.html';</script>";
            }
    }
 }else
	{
    	print "<script>alert(\"Error, asegurate que tu archivo sea .pdf y vuelve a intentarlo\"); window.location='subirl_ico.php';</script>";
    }
}
?>


<html>
	<head>
		<title>Es_Epico</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!--agregamos librerias de boostra-->
		<link async rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
		<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
		<!--segunda parte de codigo-->
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="assets/css/libro_ico.css" />
		
		
	</head>
	<body>

		<!-- Header -->
			<header id="header">
				<h1>Sube tu libro</h1>
			</header>

		<!--prueba-->
			<div class="container" id="mi_contenedor">
				<form name="subir-libro" method="post" action="" enctype="multipart/form-data">
					


					<div class="input-group">
					<span class="input-group-addon" id="basic-addon1">&#128214</span>
					<input required type="text" class="form-control" placeholder="Titulo del Libro" name="titulo" id="titulo" size="22" />
					</div>
					<BR>

					<div class="form-group">
						<textarea required class="form-control" placeholder="Descripcion del libro" row="10" id="descripcion" name="descripcion" style="color:#ffffff"></textarea>
					</div>
					<br>
					<div>
						<input type="file" name="archivolibro"/>
					</div>
					<br>
					<div>
						<input type="submit" name="subir_btn" value="Subir archivo"/>
						<button type="button" class="btn btn-danger" onclick="location.href='subirL_ico.php'">Cancelar</button>
						<button type="button" class="btn btn-link" onclick="location.href='actividades.html'"><font color="#58D3F7">Regresar</font></button>
						<!--<button type="button" class="btn btn-link" onclick="location.href='lista.php'"><font color="#58D3F7">Lista</font></button>-->
					</div>
				</form>
				


			</div>
		<!--Fin codigo-->

			

	

	
			<footer id="footer">
				<ul class="copyright">
					<li>&copy; Autor</li><li>Zitle Juarez Nancy</a></li>
				</ul>
			</footer>
	

			<script src="assets/js/main3.js"></script>

	</body>
</html>