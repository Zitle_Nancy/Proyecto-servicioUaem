<?php
/////////conexion de la base de datos //////
$host = "localhost";
$user = "Nancy";
$pw   = "root";
$db   = "portal";
$conexion = new mysqli($host,$user,$pw,$db);
if ($conexion -> connect_errno) 
{
   die("fallo la conexion:(".$conexion -> mysqli_connect_errno().")".$conexion-> mysqli_connect_errno());
}
///mostrar tabla//
$datos = "SELECT * FROM tbl_pdf";
$resultados = $conexion -> query($datos);

?>
<html lang="es">
<head>
	<title>Mostrar datos</title>
    <!--boostrap-->
        <link async rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
        <!--fin de boostrap-->
        <!--segunda parte de codigo-->
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
        <link rel="stylesheet" href="assets/css/main_mostrarlico.css" />
</head>
<body>

        <!-- Header -->
            <header id="header">
                <h1>Lista de Libros</h1>
            </header>

        <!--prueba-->
            <div class="container" id="mi_contenedor">
            <section>
            <table class="table">
            <tr >
                
                <th>Titulo</th>
                <th>Descripcion</th>
                <th>Nombre del archivo</th>
                
            </tr>
            
            <?php
            while ($registros = $resultados->fetch_array(MYSQLI_BOTH))
            { 
            ?>
            
            <tr>
                    
                   <td><?php echo $registros['titulo'];?></td>
                   <td><?php echo $registros['descripcion'];?></td>
                   <td><a href="archivo_lico.php?id=<?php echo $registros['id_documento']?>"><?php echo $registros['nombre_archivo']; ?></a></td>
                    

            </tr>
            <?php  } ?>
           

            
        </table>

    </section>
                


            </div>
        <!--Fin codigo-->
        <BR><BR>
            <div>
                        
                        <button type="button" class="btn btn-primary" onclick="location.href='actividades.html'">Regresar</button>
                        <button type="button" class="btn btn-link" onclick="location.href='index.html'"><font color="#FFFF00">Inicio</font></button>
                        
            </div>
            
        <BR><BR>
    

    
            <footer id="footer">
                <ul class="copyright">
                    <li>&copy; Autor</li><li>Zitle Juarez Nancy</a></li>
                </ul>
            </footer>
    

            <script src="assets/js/main3.js"></script>

    </body>
</html>